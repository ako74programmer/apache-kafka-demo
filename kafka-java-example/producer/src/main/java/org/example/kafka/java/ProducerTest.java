package org.example.kafka.java;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerTest {

    private static final Logger logger = LoggerFactory.getLogger(ProducerTest.class);

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "127.0.0.1:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");


        try(KafkaProducer<String, String> producer = new KafkaProducer<>(props)) {
            for (int i = 0; i < 100; i++) {
                String msg = "Message " + i;

                ProducerRecord<String, String> producerRecord =
                        new ProducerRecord<>("second_kafka_topic", msg);

                RecordMetadata recordMetadata =
                       producer.send(producerRecord).get();


                String formatMsg = String.format("Sent record : value %s to partition %s", producerRecord.value(), recordMetadata.partition());
                logger.info(formatMsg);
            }
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Interrupted!", e);
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }

    }

}
