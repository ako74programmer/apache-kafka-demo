package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerThread implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(ProducerThread.class);

    private final KafkaProducer<String, String> producer;
    private final String topic;

    public ProducerThread(String brokers, String topic) {
        Properties prop = createProducerConfig(brokers);
        this.producer = new KafkaProducer<>(prop);
        this.topic = topic;
    }

    private static Properties createProducerConfig(String brokers) {
        Properties props = new Properties();
        props.put("bootstrap.servers", brokers);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    @Override
    public void run() {
        logger.info("Produces 3 messages");
        for (int i = 0; i < 5; i++) {
            String msg = "Message " + i;
            producer.send(new ProducerRecord<>(topic, msg), (metadata, e) -> {
                if (e != null) {
                    logger.error("Error: ", e);
                }
                logger.info("Sent: {}, Partition: {}, Offset: {}", msg, metadata.partition(), metadata.offset());
            });

        }
        // closes producer
        producer.close();
    }

    public static void main(String[] args) {
        String brokers = "127.0.0.1:9092";
        String topic = "second_kafka_topic";
        // Start Producer Thread
        ProducerThread producerThread = new ProducerThread(brokers, topic);
        Thread t1 = new Thread(producerThread);
        t1.start();
    }
}
