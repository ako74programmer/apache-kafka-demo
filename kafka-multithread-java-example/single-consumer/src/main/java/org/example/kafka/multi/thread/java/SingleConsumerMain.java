package org.example.kafka.multi.thread.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SingleConsumerMain {
    private static final Logger logger = LoggerFactory.getLogger(SingleConsumerMain.class);

    public static void main(String[] args) {

        String brokers = "127.0.0.1:9092";
        String groupId = "group01";
        String topic = "second_kafka_topic";
        int numberOfThread = 3;

        SingleConsumer consumer = new SingleConsumer(brokers, groupId, topic);
        consumer.execute(numberOfThread);

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            logger.error("Error: ", e);
            Thread.currentThread().interrupt();
        }
        consumer.shutdown();
    }
}
