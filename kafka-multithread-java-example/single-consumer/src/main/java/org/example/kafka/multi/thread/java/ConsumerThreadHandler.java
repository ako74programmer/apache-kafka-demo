package org.example.kafka.multi.thread.java;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerThreadHandler implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerThreadHandler.class);

    private final ConsumerRecord<String, String> consumerRecord;

    public ConsumerThreadHandler(ConsumerRecord<String, String> consumerRecord) {
        this.consumerRecord = consumerRecord;
    }

    public void run() {
        String msg = "Process: " + consumerRecord.value() + ", Offset: " + consumerRecord.offset()
                + ", By ThreadID: " + Thread.currentThread().getId();
        logger.info(msg);
    }
}
