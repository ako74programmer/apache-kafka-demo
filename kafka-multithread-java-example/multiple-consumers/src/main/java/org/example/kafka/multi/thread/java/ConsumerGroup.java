package org.example.kafka.multi.thread.java;

import java.util.ArrayList;
import java.util.List;

public class ConsumerGroup {
    private final int numberOfConsumers;
    private final String groupId;
    private final List<ConsumerThread> consumers;

    public ConsumerGroup(String brokers, String groupId, String topic,
                                     int numberOfConsumers) {
        this.groupId = groupId;
        this.numberOfConsumers = numberOfConsumers;
        consumers = new ArrayList<>();
        for (int i = 0; i < this.numberOfConsumers; i++) {
            ConsumerThread ncThread =
                    new ConsumerThread(brokers, this.groupId, topic);
            consumers.add(ncThread);
        }
    }

    public void execute() {
        for (ConsumerThread ncThread : consumers) {
            Thread t = new Thread(ncThread);
            t.start();
        }
    }

    public int getNumberOfConsumers() {
        return numberOfConsumers;
    }

    public String getGroupId() {
        return groupId;
    }

}
