package org.example.kafka.multi.thread.java;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class MultipleConsumersMain {
    private static final Logger logger = LoggerFactory.getLogger(MultipleConsumersMain.class);

    public static void main(String[] args) {

        String brokers = "127.0.0.1:9092";
        String groupId = "group01";
        String topic = "second_kafka_topic";
        int numberOfConsumer = 3;


        // Start group of Notification Consumers
        ConsumerGroup consumerGroup =
                new ConsumerGroup(brokers, groupId, topic, numberOfConsumer);

        consumerGroup.execute();

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
           logger.error("Error: ", e);
           Thread.currentThread().interrupt();
        }
    }
}
