package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.LoggerFactory;

public class Dispatcher implements Runnable {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Dispatcher.class);

    private final KafkaProducer<String, String> producer;
    private final String topicName;

    Dispatcher(KafkaProducer<String, String> producer, String topicName) {
        this.producer = producer;
        this.topicName = topicName;
    }

    @Override
    public void run() {
        logger.info("Start sending messages");
        for (int i = 0; i < 5000; i++) {
            String msg = "Message " + i;
            producer.send(new ProducerRecord<>(topicName, msg), (metadata, e) -> {
                if (e != null) {
                    logger.error("Error: ", e);
                }
                logger.info("Sent: {}, Partition: {}, Offset: {}", msg, metadata.partition(), metadata.offset());
            });

        }
        logger.info("Finished sending messages");

    }
}
