package org.example;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class MultithreadedProducer {
    private static final Logger logger = LoggerFactory.getLogger(MultithreadedProducer.class);

        public static void main(String[] args) {
        String brokers = "127.0.0.1:9092";
        String topicName = "second_kafka_topic";

        Properties properties = new Properties();
        properties.put("bootstrap.servers", brokers);

        properties.put(ProducerConfig.ACKS_CONFIG, "1");                  //properties.put("acks", "all");
        properties.put(ProducerConfig.BATCH_SIZE_CONFIG, 16_384 * 4);     //properties.put("batch.size", 16384);
        properties.put(ProducerConfig.LINGER_MS_CONFIG, 200);             //properties.put("linger.ms", 1);
        properties.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");

        properties.put("retries", 0);
        properties.put("buffer.memory", 33554432);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        logger.info("Starting dispatcher threads...");
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        int length = 2;
        Thread[] dispatchers = new Thread[length];
        for (int i = 0; i < length; i++) {
            dispatchers[i] = new Thread(new Dispatcher(producer, topicName));
            dispatchers[i].start();
        }

        try {
            for (Thread t : dispatchers)
                t.join();
        } catch (InterruptedException e) {
            logger.error("Thread Interrupted ");
            Thread.currentThread().interrupt();
        } finally {
            producer.close();
            logger.info("Finished dispatcher demo - Closing Kafka Producer.");
        }
    }
}
